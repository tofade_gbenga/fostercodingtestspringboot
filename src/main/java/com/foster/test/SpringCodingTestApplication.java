package com.foster.test;

import com.foster.test.Model.AgeRating;
import com.foster.test.Processors.ReaderParser;
import com.foster.test.Processors.WriterParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;

@SpringBootApplication
public class SpringCodingTestApplication implements CommandLineRunner {

    private static Logger LOGGER = LoggerFactory.getLogger(SpringCodingTestApplication.class);

    @Autowired
    private ReaderParser readerParser;

    @Autowired
    private WriterParser writerParser;

    /**
     * Execution Entry Point
     * @param args
     */
	public static void main(String[] args) {
		SpringApplication.run(SpringCodingTestApplication.class, args);
	}

    /**
     * Callback used to run the bean.
     *
     * @param args incoming main method arguments
     * @throws Exception on error
     */
    @Override
    public void run(String... args) throws Exception {
        LOGGER.info("Started Processing Asset Data");
        File jsonFile = new File(String.join(File.separator, Paths.get("").toAbsolutePath().toString(), "assets.json"));

        try {
            readerParser.setFile(jsonFile);
            readerParser.processFile();
            readerParser.close();
            Map<String, AgeRating> result = readerParser.getResultMap();

            File resultFile = new File(String.join(File.separator, Paths.get("").toAbsolutePath().toString(), "result.json"));
            if (!resultFile.exists()){
                resultFile.createNewFile();
            }

            writerParser.setParserFile(resultFile);
            writerParser.writeToJson(result);
            writerParser.flushAndCloseWriter();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
