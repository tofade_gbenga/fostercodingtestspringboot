package com.foster.test.Model;

import java.util.ArrayList;
import java.util.List;

public class AgeRating {
    private String FSK;
    private int count;
    private final List<Movie> movies = new ArrayList<>();

    public String getFSK() {
        return FSK;
    }

    public void setFSK(String FSK) {
        this.FSK = FSK;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void addMovie(Movie movie) {
        this.movies.add(movie);
    }


}
