package com.foster.test.Processors;

import com.foster.test.Model.AgeRating;
import com.foster.test.Model.Movie;
import com.foster.test.SpringCodingTestApplication;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ReaderParser {
    private JsonReader jsonReader;
    private Map<String, AgeRating> resultMap = new HashMap<>();
    private Logger LOGGER = LoggerFactory.getLogger(ReaderParser.class);

    /**
     * Component constructor
     */
    public ReaderParser() {
    }

    /**
     * A constructor that accepts a file and instantiates a JSON Reader
     *
     * @param file
     * @throws FileNotFoundException on error
     */
    public ReaderParser(File file) throws FileNotFoundException {
        this.jsonReader = new JsonReader(new FileReader(file));
    }

    /**
     * set the file to process
     *
     * @param file
     */
    public void setFile(File file) {
        if (this.jsonReader == null) {
            try {
                this.jsonReader = new JsonReader(new FileReader(file));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            LOGGER.error("File already read and Json Reader initialized");
        }

    }

    /**
     * Process the reader and build result
     *
     * @throws IOException on error
     */
    public void processFile() throws IOException {
        this.jsonReader.beginArray();
        while (this.jsonReader.hasNext()) {
            JsonToken assetToken = this.jsonReader.peek();

            if (assetToken.equals(JsonToken.BEGIN_OBJECT))
                buildMovieAsset(this.jsonReader);
        }
        this.jsonReader.endArray();
    }

    /**
     * Build the movie asset from the current reader object
     *
     * @param jsonReader
     */
    private void buildMovieAsset(JsonReader jsonReader) {
        try {
            jsonReader.beginObject();
            Movie movie = new Movie();
            Boolean isMovie = Boolean.FALSE;
            List<String> fskList = new ArrayList<>();

            while (jsonReader.hasNext()) {
                String propName = jsonReader.nextName();


                switch (propName.toLowerCase()) {
                    case "id":
                        movie.setId(jsonReader.nextInt());
                        break;
                    case "title":
                        movie.setTitle(jsonReader.nextString());
                        break;
                    case "production_year":
                        movie.setYear(jsonReader.nextInt());
                        break;
                    case "object_class":
                        isMovie = jsonReader.nextString().equalsIgnoreCase("Movie");
                        break;
                    case "fsk_level_list_facet":
                        jsonReader.beginArray();
                        while (jsonReader.hasNext()) {
                            fskList.add(jsonReader.nextString().strip().toUpperCase());
                        }
                        jsonReader.endArray();
                        break;
                    default:
                        jsonReader.skipValue();
                        break;


                }

            }

            if (isMovie) {
                addToMap(fskList, movie);
            }
            jsonReader.endObject();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Close JSON Reader after Usage
     */
    public void close() {
        try {
            this.jsonReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Add movie to the number of fsk found then add to map
     *
     * @param fsks
     * @param movie
     */
    private void addToMap(List<String> fsks, Movie movie) {
        fsks.forEach(fsk -> {
            AgeRating rating = this.resultMap.get(fsk);
            if (rating != null) {
                rating.setCount(rating.getCount() + 1);
            } else {
                rating = new AgeRating();
                rating.setCount(1);
                rating.setFSK(fsk);
                this.resultMap.put(fsk, rating);
            }
            rating.addMovie(movie);

            LOGGER.info(String.format("Processed movie with id: %s", movie.getId()));
        });
    }

    /**
     * Get the result map
     *
     * @return result of the age rating map
     */
    public Map<String, AgeRating> getResultMap() {
        return this.resultMap;
    }

}
