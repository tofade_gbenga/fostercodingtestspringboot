package com.foster.test.Processors;

import com.foster.test.Model.AgeRating;
import com.foster.test.SpringCodingTestApplication;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Map;

@Component
public class WriterParser {
    private Gson gsonBuilder;
    private FileWriter fileWriter;
    private Logger LOGGER = LoggerFactory.getLogger(WriterParser.class);

    /**
     * Component Constructor
     */
    public WriterParser() {
    }

    /**
     * Constructor to instantiate the Json stream parse to the result file specified
     * @param resultFile
     */
    public WriterParser(File resultFile) throws IOException {
        this.gsonBuilder = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        this.fileWriter = new FileWriter(resultFile);
    }


    /**
     * Creates a Gson Builder and File Writes with the passed file
     * @param resultFile
     * @throws IOException on error
     */
    public void setParserFile(File resultFile) throws IOException {
        if (this.fileWriter == null || this.gsonBuilder == null) {
            try {
                this.gsonBuilder = new GsonBuilder()
                        .setPrettyPrinting()
                        .create();
                this.fileWriter = new FileWriter(resultFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            LOGGER.error("File already Parsed and FileWriter initialized");
        }
    }


    /**
     * Writes Json to file
     * @param ratings
     * @throws IOException
     */
    public void writeToJson(Map<String, AgeRating> ratings) throws IOException {
        this.gsonBuilder.toJson(ratings, fileWriter);
    }

    /**
     * Flushes and cleans the fileWriter
     */
    public void flushAndCloseWriter(){
        try {
            this.fileWriter.flush();
            this.fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
